@extends('layout')

@section('content')
    <div class="well no-padding" style="width: 100%;">
        <div style="display: flex; justify-content: center; margin: 20px 0">
            <div style="padding: 20px; background-color: white; border-radius: 5px; color:black">Ваш заказ успешно оплачен!</div>
        </div>
    </div>
@endsection