@extends('layout')

@section('content')
    <div class="well no-padding" style="width: 100%;">
        <div style="display: flex; justify-content: center; margin: 20px 0">
            <div style="padding: 20px; background-color: white; border-radius: 5px; color:black">Пожалуйста подождите, происходит формирование счета...</div>
        </div>

        <form id="payment" method="post" action="https://wl.walletone.com/checkout/checkout/Index">
            @foreach($fields as $name => $field)
                @if(is_array($field))
                    @foreach($field as $item)
                        <input type="hidden" name="{{ $name }}" value="{{ $item }}">
                    @endforeach
                @else
                    <input type="hidden" name="{{ $name }}" value="{{ $field }}">
                @endif
            @endforeach
        </form>
    </div>

    <script>
        $(document).ready(function () {
            setTimeout(function () {
                $('#payment').submit();
            }, 300);
        });
    </script>
@endsection